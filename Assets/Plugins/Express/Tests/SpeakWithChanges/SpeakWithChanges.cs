﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using ExpressPlugin;

public class SpeakWithChanges : MonoBehaviour
{
  public Text _volumeLabel;
  public Slider _volume;
  public Text _rateLabel;
  public Slider _rate;
  public GameObject _prefab;

  public ExpressSettings _settings;

  void Awake()
  {
    Express.Initialize(_settings);
    Express.EnableVisualizer(true);
    OnVolumeChange(100);
    OnRateChange(0);
  }

  public void OnVolumeChange(float val)
  {
    _volumeLabel.text = "Volume: " + val.ToString();
    Express.SetVolume((int)val);
    _volume.value = val;
  }

  public void OnRateChange(float val)
  {
    _rateLabel.text = "Rate: " + val.ToString();
    Express.SetRate((int)val);
    _rate.value = val;
  }

  public void SpeakALot()
  {
    Express.Speak("this is a very long string of words to give the user enough time to pres the button below this");
  }

  public void Stop()
  {
    Express.Silence();
  }
}
