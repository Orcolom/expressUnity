﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using ExpressPlugin;

namespace ExpressPlugin.Tests
{
  public class SpeakOnStart : MonoBehaviour
  {
    public ExpressSettings _settings;
    void Start()
    {
      Express.Initialize(_settings);
      Express.Speak("This says something about meta");
    }
  }
}
