﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using ExpressPlugin.Internal;


namespace ExpressPlugin.Internal
{
  [CustomEditor(typeof(ExpressMetadata))]
  public class ExpressMetadataEditor : ExpressElementEditor
  {
    bool _editName;
    bool _editState;
    bool _editInfo;
    bool _editRole;

    public void OnEnable()
    {
      var data = (ExpressMetadata)target;
      if (data.Metadata == null)
        data.Metadata = new Metadata();

      // if there is data in a position then see it as editing
      _editName = data.Metadata.Name.Length != 0;
      _editRole = data.Metadata.Role.Length != 0;
      _editInfo = data.Metadata.Info.Length != 0;
      _editState = data.Metadata.State.Length != 0;
    }

    public override void OnInspectorGUI()
    {
      if (!IsRootElement())
        return;

      var guiState = GUI.enabled;

      var data = (ExpressMetadata)target;
      var defaultData = Express.GetMetaData(data.gameObject);
      var root = Express.GetUIElementRoot(data.gameObject);


      Field("Role", defaultData.Role, ref data.Metadata.Role, ref _editRole);
      Field("Name", defaultData.Name, ref data.Metadata.Name, ref _editName);
      Field("Info", defaultData.Info, ref data.Metadata.Info, ref _editInfo);
      Field("State", defaultData.State, ref data.Metadata.State, ref _editState);

      GUI.enabled = guiState;
    }

    void Field(string label, string defaultData, ref string data, ref bool editing)
    {
      GUILayout.BeginHorizontal();

      GUI.enabled = true;
      var state = EditorGUILayout.Toggle(editing, GUILayout.Width(15));

      // if the state of the toggle changed      
      if (editing && !state)
        data = "";
      else if (state && !editing)
        data = defaultData;
      editing = state;


      GUI.enabled = editing;
      EditorGUILayout.LabelField(label, GUILayout.Width(50));
      
      // depending on the state show info
      if (editing)
        data = EditorGUILayout.TextField("", data);
      else
        EditorGUILayout.TextField("", defaultData);
      
      GUILayout.EndHorizontal();

      if (data == "" && editing)
        data = " ";
    }
  }
}
