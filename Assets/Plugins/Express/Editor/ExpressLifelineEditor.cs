﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using ExpressPlugin.Internal;


namespace ExpressPlugin.Internal
{
  [CustomEditor(typeof(ExpressLifeline))]
  public class ExpressLifelineEditor : Editor
  {
    public override void OnInspectorGUI()
    {
      EditorGUILayout.HelpBox("This script will get added and destroyed at runtime by express. You can ignore this file at runtime or remove it in the editor", MessageType.Info);
    }
  }
}
