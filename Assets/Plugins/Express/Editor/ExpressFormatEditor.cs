﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

using ExpressPlugin;
using ExpressPlugin.Internal;

namespace ExpressPlugin.Internal
{
  [CustomEditor(typeof(ExpressFormat))]
  public class ExpressFormatEditor : ExpressElementEditor
  {
    SerializedProperty _textField;
    SerializedProperty _sourceField;

    public void OnEnable()
    {
      _textField = serializedObject.FindProperty("Text");
      _sourceField = serializedObject.FindProperty("Source");
    }

    public override void OnInspectorGUI()
    {
      if (!IsRootElement())
        return;

      _textField.stringValue = EditorGUILayout.TextArea(_textField.stringValue);
      EditorGUILayout.PropertyField(_sourceField);
      
      GUI.enabled = false;
      EditorGUILayout.TextField((target as ExpressFormat).GetMetaData().Info);
      GUI.enabled = true;

      serializedObject.ApplyModifiedProperties();
    }
  }
}
