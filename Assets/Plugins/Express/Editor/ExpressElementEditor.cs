﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using ExpressPlugin.Internal;


namespace ExpressPlugin.Internal
{
  [CustomEditor(typeof(AExpressElement), false)]
  public class ExpressElementEditor : Editor
  {
    public override void OnInspectorGUI()
    {
      IsRootElement();
    }

    public bool IsRootElement()
    {
      var data = (AExpressElement)target;
      var root = Express.GetUIElementRoot(data.gameObject);
      
      var res = root != data.gameObject; 
      if (res)
      {
        EditorGUILayout.HelpBox(data.GetType().Name + " only applies for root Elements. This will get ignored", MessageType.Warning);
        EditorGUILayout.LabelField("Element root according to express:");
        GUI.enabled = false;
        EditorGUILayout.ObjectField(root, typeof(GameObject), true);
        GUI.enabled = true;
      }

      return !res;
    }
  }
}
