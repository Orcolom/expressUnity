﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace ExpressPlugin.Internal
{
  public class ExpressEditorWindow : EditorWindow
  {
    // Add menu named "My Window" to the Window menu
    [MenuItem("Window/Analysis/Express Viewer", false)]
    static void Init()
    {
      ExpressEditorWindow window = (ExpressEditorWindow)EditorWindow.GetWindow(typeof(ExpressEditorWindow));
      window.name = "Express Viewer";
      window.Show();
    }

    void OnGUI()
    {
      GUILayout.Label("Element", EditorStyles.boldLabel);

      var go = Selection.activeGameObject;
      if (go == null)
      {
        EditorGUILayout.HelpBox("When an UI element is selected this will tell you what info gets passed to express", MessageType.Info);
        return;
      }

      EditorGUILayout.BeginHorizontal();
      EditorGUILayout.LabelField("selected object", GUILayout.Width(100));
      GUI.enabled = false;
      EditorGUILayout.ObjectField(go, typeof(GameObject), true);
      GUI.enabled = true;
      EditorGUILayout.EndHorizontal();

      var root = Express.GetUIElementRoot(go);
      if (root == null)
      {
        EditorGUILayout.HelpBox("The selected gameobject isn't an valid object", MessageType.Warning);
        return;
      }

      EditorGUILayout.BeginHorizontal();
      EditorGUILayout.LabelField("root", GUILayout.Width(100));
      GUI.enabled = false;
      EditorGUILayout.ObjectField(root, typeof(GameObject), true);      
      GUI.enabled = true;
      EditorGUILayout.EndHorizontal();


      var data = Express.GetMetaData(root);
      GUILayout.Label("Metadata", EditorStyles.boldLabel);

      EditorGUILayout.BeginHorizontal();
      EditorGUILayout.LabelField("Role", GUILayout.Width(50));
      GUI.enabled = false;
      EditorGUILayout.TextField(data.Role);
      GUI.enabled = true;
      EditorGUILayout.EndHorizontal();

      EditorGUILayout.BeginHorizontal();
      EditorGUILayout.LabelField("Name", GUILayout.Width(50));
      GUI.enabled = false;
      EditorGUILayout.TextField(data.Name);
      GUI.enabled = true;
      EditorGUILayout.EndHorizontal();

      EditorGUILayout.BeginHorizontal();
      EditorGUILayout.LabelField("Info", GUILayout.Width(50));
      GUI.enabled = false;
      EditorGUILayout.TextField(data.Info);
      GUI.enabled = true;
      EditorGUILayout.EndHorizontal();

      EditorGUILayout.BeginHorizontal();
      EditorGUILayout.LabelField("State", GUILayout.Width(50));
      GUI.enabled = false;
      EditorGUILayout.TextField(data.State);
      GUI.enabled = true;
      EditorGUILayout.EndHorizontal();      
    
      var expressElement = root.GetComponent<AExpressElement>();
      if (expressElement == null)
        return;
      
      GUILayout.Label("Controlled (partly) by", EditorStyles.boldLabel);
      EditorGUILayout.LabelField(expressElement.GetType().Name);
      EditorGUILayout.ObjectField(expressElement, expressElement.GetType(), true);      
    }


    public void Update()
    {  
     Repaint();
    }
  }
}
