﻿using System;
using System.Linq;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;
using UnityEngine.EventSystems;
using ExpressPlugin;

namespace ExpressPlugin.EventSystems
{
  public class ExpressInputModule : ModifiedStandaloneInputModule
  {
    public ExpressSettings _settings;
    public Canvas _canvas;

    protected GameObject _lastGameObject = null;
    protected InputField _input;
    protected Dropdown _dropDown;
    protected string _inputText;
    float _timeLastClick = 0;
    bool _hasLingringClick = false;

    protected new void Awake()
    {
      if (_settings.InitializeExpressOnAwake)
        Express.Initialize(_settings);
      
      if (_canvas != null)
      {
        var element = Express.FindFirstElement(_canvas.gameObject);
        if (element != null)
          ExpressProcessUpdate(element);
      }

      base.Awake();
    }

    void ExpressProcessUpdate(GameObject go)
    {
      // get the root element
      go = Express.GetUIElementRoot(go);

      if (go == null)
        return;

      // is a new element
      if (go != _lastGameObject)
      {
        if (!Express.ShouldIgnoreGameObject(go))
        {
          // if its a new element speak
          Express.SpeakGameObject(go);
          Express.SetVisualizerTarget(go.transform as RectTransform);
          EventSystem.current.SetSelectedGameObject(go);
          
          _lastGameObject = go;

          if (_dropDown != null)
          {
            // when dropdown gets unselected
            // this can mean that it was opened up
            _dropDown.StartCoroutine(FindBlocker());
            _dropDown = null;
          }
        }
      }

      // if dropdown and it got selected then find the blocker
      // TODO: change this to when clicked on
      var dropDown = go.GetComponent<Dropdown>();
        
      if (dropDown != null && dropDown != _dropDown)
      {
        _dropDown = dropDown;
        dropDown.StartCoroutine(FindBlocker());
      }
    }

    IEnumerator FindBlocker()
    {
      // dropdown creates an button in the background that is clickable
      // but they dont save this anywhere, the only way to find it is by looking by name

      // it takes some time before unity finds the blocker
      GameObject blocker = GameObject.Find("Blocker");

      if (blocker == null)
      {
        //so keep looking for it every frame until it finds it
        int count = 0;
        do
        {
          yield return new WaitForEndOfFrame();
          count++;
          var blockers = GameObject.FindObjectsOfType<Canvas>();
          foreach (var b in blockers)
          {
            if (b.name == "Blocker")
            {
              blocker = b.gameObject;
              break;
            }
          }
        }
        while (blocker == null && count < 100);
        // print(count);
      }

      // if found add the metadata info
      if (blocker != null)
      {
        var metadata = blocker.GetComponent<ExpressMetadata>();
        if (metadata == null)
        {
          metadata = blocker.AddComponent<ExpressMetadata>();
          metadata.Metadata = new Metadata();
          metadata.Metadata.Name = "Exit Dropdown";
        }
      }
    }

    void ExpressProcessInput(GameObject go)
    {
      // add a listener to the input field for when you type
      var input = go.GetComponent<InputField>();
      if (_input != input)
      {
        if (_input != null)
          _input.onValueChanged.RemoveListener(ExpressOnInputChange);

        if (input != null)
        {
          _inputText = input.text;
          input.onValueChanged.AddListener(ExpressOnInputChange);
        }

        _input = input;
      }
    }

    void ExpressOnInputChange(string str)
    {
      // detect what changed in input
      Express.Speak(str, true);
    }



    #region Tabbing
    private void Tabbing()
    {
      if (_settings.UseTabbing && _lastGameObject != null)
      {
        if (TriggeredTabPrev())
        {
          var element = Express.FindPrevElement(_lastGameObject);
          if (element != null)
            ExpressProcessUpdate(element);
        }
        else if(TriggeredTabNext())
        {
          var element = Express.FindNextElement(_lastGameObject);
          if (element != null)
            ExpressProcessUpdate(element);
        }
      }
    }

    private bool TriggeredTabNext()
    {
      if (Input.GetKeyDown(KeyCode.Tab) && !(Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)))
        return true;
      
      if (input.touchCount == 1)
      {
        Touch touch = input.GetTouch(0);
        if (touch.phase == TouchPhase.Ended)
        {
          if (touch.deltaPosition.x > 8)
            return true;
        }
      }
      
      return false;
    }
    
    private bool TriggeredTabPrev()
    {
      if (Input.GetKeyDown(KeyCode.Tab) && (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)))
        return true;
      
      if (input.touchCount == 1)
      {
        Touch touch = input.GetTouch(0);
        if (touch.phase == TouchPhase.Ended)
        {
          if (touch.deltaPosition.x < -8)
            return true;
        }
      }

      return false;
    }        
    #endregion
    
    protected override bool ProcessTouchEvents()
    {
      Tabbing();

      if (_settings.ForceDoubleClick)
      {
        if (TriggeredSelect(null))
          return base.ProcessTouchEvents();
        return false;
      }

      return base.ProcessTouchEvents();
    }

    public override void Process()
    {
      // Tabbing();
      base.Process();
    }


    bool TriggeredSelect(MouseButtonEventData leftButtonData)
    {
      var now = Time.time;
      var timeSinceLastClick = Mathf.Abs(_timeLastClick - now);
      bool doubleClick = false;

      bool clicked = false;
      if (leftButtonData == null)
      {
        if (input.touchCount == 1)
        {
          Touch touch = input.GetTouch(0);
          if (touch.phase == TouchPhase.Ended && !(TriggeredTabNext() || TriggeredTabPrev()))
            clicked = true;
        }
      } else if(leftButtonData.ReleasedThisFrame())
        clicked = true;
      
      if (!clicked)
        return false;

      // Debug.LogFormat("clicked {0} {1}", timeSinceLastClick, _hasLingringClick);
      if (_hasLingringClick && timeSinceLastClick < repeatDelay)
      {
        //second click
        print("double click " + timeSinceLastClick);
        _hasLingringClick = false;
        doubleClick = true;
        _timeLastClick = 0;
      }
      else
      {
        // first click
        _hasLingringClick = true;
        // print("- is lingring click");
        timeSinceLastClick = 0;
        _timeLastClick = 0;
      }

      // ended single click
      if (_hasLingringClick && timeSinceLastClick > repeatDelay)
      {
        _hasLingringClick = false;
        _timeLastClick = 0;
      }
      
      _timeLastClick = now;

      if (doubleClick)
        return true;
      return false;
    }

    protected override void ProcessMouseEvent(int id)
    {
      var mouseData = GetMousePointerEventData(id);
      var leftButtonData = mouseData.GetButtonState(PointerEventData.InputButton.Left).eventData;
      var go = leftButtonData.buttonData.pointerCurrentRaycast.gameObject;
      ExpressProcessUpdate(go); // speak while hover 

      if (_settings.ForceDoubleClick)
      {
        if (TriggeredSelect(leftButtonData))
          base.ProcessMouseEvent(mouseData);
      }
      else
        base.ProcessMouseEvent(mouseData);
    }

    GameObject prev = null;
    protected override bool SendUpdateEventToSelectedObject()
    {
      if (eventSystem.currentSelectedGameObject == null)
        return false;

      var data = GetBaseEventData();
      ExecuteEvents.Execute(eventSystem.currentSelectedGameObject, data, ExecuteEvents.updateSelectedHandler);

      if (eventSystem.currentSelectedGameObject != prev)
      {
        prev = eventSystem.currentSelectedGameObject;
        // print(eventSystem.currentSelectedGameObject);
      }

      ExpressProcessUpdate(eventSystem.currentSelectedGameObject); //speak when change
      ExpressProcessInput(eventSystem.currentSelectedGameObject); // interact with input

      return data.used;
    }

    protected override void ProcessDrag(PointerEventData pointerEvent)
    {
      base.ProcessDrag(pointerEvent);

      // make slider talk on change
      if (pointerEvent.dragging)
      {
        if (_lastGameObject != null)
        {
          var toggle = _lastGameObject.GetComponent<Slider>();
          if (toggle != null)
            Express.SpeakGameObject(toggle.gameObject);
        }
      }
    }
  }
}