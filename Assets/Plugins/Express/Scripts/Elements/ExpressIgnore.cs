﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using ExpressPlugin;

public class ExpressIgnore : AExpressElement
{
  public bool IsChildOfElement;
  
  public override Metadata GetMetaData()
  {
    return new Metadata()
    {
      Name = " ",
      State = " ",
      Role = " ",
      Info = " "
    };
  }
}
