﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ExpressPlugin
{
  // attractable to gameobjects will override default settings
  public class ExpressMetadata :AExpressElement
  {
    public Metadata Metadata;

    public override Metadata GetMetaData()
    {
      return Metadata;
    }
  }
}
