﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System.Text.RegularExpressions;

namespace ExpressPlugin
{
  // attractable to gameobjects will override default settings
  [System.Serializable]
  public class ExpressStringSource : SerializableCallback<string>  {}

  public class ExpressFormat : AExpressElement
  {
    public string Text = "";
    public ExpressStringSource Source = new ExpressStringSource();

    public override Metadata GetMetaData()
    {
      var str = Text;
      if (Regex.Matches(Text, "{\\d+}").Count > 0)
      {
        var val = Source.Invoke();
        if (val == null)
          val = "";

        try
        {
          str = string.Format(Text, val);
        }
#pragma warning disable 0168 // ignore e is unused
        catch(System.FormatException e)
        {
          str = Text;
        }
#pragma warning restore 0168
      }
      
      return new Metadata()
      {
        Role = " ",
        Name = " ",
        State = " ",
        Info = str,
      };
    }
  }
}
