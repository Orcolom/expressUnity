﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using ExpressPlugin;

public abstract class AExpressElement : MonoBehaviour
{
  public abstract Metadata GetMetaData();
}
