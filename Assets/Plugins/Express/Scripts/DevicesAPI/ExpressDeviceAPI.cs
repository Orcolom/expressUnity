﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ExpressDeviceAPI
{
  
  /// <summery> Initialization Function
  /// <para> Sync function to initialize anything that needs to be initialized </para>
  /// <returns> true if initialization was successfull </returns>
  /// </summery>
  public abstract bool Initialize();
  
  /// <summery> Destroy
  /// <para> Sync function to destroy anything that needs to be destroyed </para>
  /// </summery>
  public abstract void Destroy(); 

  /// <summery> Say the provided text
  /// <param name="str">text to say</param>
  /// <param name="intercept">interupt current executed text and clear the back log</param>
  /// <returns> true if executed successfully </returns>
  /// </summery>
  public abstract bool Speak(string str, bool intercept = false);  
  
  /// <summery> End the current executed text and clear the backlog </summery>
  public abstract void Silence();

  /// <summery> Set the voice volume
  /// <param name="volume">the new volume between 0 and 100</param>
  /// </summery>
  public abstract void SetVolume(int volume);

  /// <summery> Get the voice volume
  /// <returns> current volume between 0 and 100</returns>
  /// </summery>
  public abstract int GetVolume();
  
  /// <summery> Set the speaking rate
  /// <param name="volume">the new rate between -10 and 10</param>
  /// </summery>
  public abstract void SetRate(int rate);

  /// <summery> Get the speaking rate
  /// <returns> current rate between -10 and 10</returns>
  /// </summery>
  public abstract int GetRate();
  
}
