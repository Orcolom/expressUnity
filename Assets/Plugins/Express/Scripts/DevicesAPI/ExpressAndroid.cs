﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace ExpressPlugin.Devices
{
  public class ExpressAndroid : ExpressDeviceAPI
  {
    public static string arrName = "com.orcolom.expressandroid.Plugin";
    static AndroidJavaClass _pluginClass;

    bool _isInitialized = false;

    public override bool Initialize()
    {
      if (_isInitialized)
        return true;
      
      _pluginClass = new AndroidJavaClass(arrName);

      int result = _pluginClass.CallStatic<int>("initExpress");
      _isInitialized = result == 0;

      return _isInitialized;
    }

    public override void Destroy()
    {
      if (!_isInitialized)
        return;
      
      _pluginClass.CallStatic("destroyExpress");
      
      _isInitialized = false;
    }

    public override bool Speak(string str, bool intercept = false)
    {
      if (!_isInitialized)
      {
        var init = Initialize();
        if (!init)
          return false;
      }

      int result = _pluginClass.CallStatic<int>("speak", str, intercept);
      return result == 0;
    }

    public override void Silence()
    {
      if (!_isInitialized)
        Initialize();

      _pluginClass.CallStatic("silence");
    }

    public override void SetVolume(int volume)
    {  
      if (!_isInitialized)
        Initialize();

      _pluginClass.CallStatic("setVolume", volume);
    }

    public override int GetVolume()
    {
      if (!_isInitialized)
        Initialize();

      return _pluginClass.CallStatic<int>("getVolume");
    }

    public override void SetRate(int rate)
    {
      if (!_isInitialized)
        Initialize();

      _pluginClass.CallStatic("setRate", rate);
    }

    public override int GetRate()
    {
      if (!_isInitialized)
        Initialize();

      return _pluginClass.CallStatic<int>("getRate");
    }
  }
}