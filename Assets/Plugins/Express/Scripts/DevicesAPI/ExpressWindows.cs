﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace ExpressPlugin.Devices
{
  public class ExpressWindows : ExpressDeviceAPI
  {
    // windows exports
    #region dll_exports

    [DllImport("Express-Win")] // returns hrResult
    static extern long initExpress();

    [DllImport("Express-Win")]
    static extern void destroyExpress();

    [DllImport("Express-Win")] // returns hrResult
    static extern long speak(string s, bool intercept);

    [DllImport("Express-Win")]
    static extern void silence();

    [DllImport("Express-Win")]
    static extern void setVolume(int volume);
    
    [DllImport("Express-Win")]
    static extern long getVolume();

    [DllImport("Express-Win")]
    static extern void setRate(int rate);
    
    [DllImport("Express-Win")]
    static extern long getRate();

    #endregion dll_exports



    bool _isInitialized = false;

    public override bool Initialize()
    {
      if (_isInitialized)
        return true;
      
      // init will return hrResult, anything except 0 is considered failed initializing
      var res = initExpress();
      if (res != 0)
        return false;

      _isInitialized = true;
      return _isInitialized;
    }

    public override void Destroy()
    {
      if (!_isInitialized)
        return;
      
      destroyExpress();
      _isInitialized = false;
    }

    public override bool Speak(string str, bool intercept = false)
    {
      if (!_isInitialized)
      {
        var res = Initialize();
        if (!res)
          return res;
      }

      // speak will return hrResult, anything except 0 is considered a fail
      return speak(str, intercept) == 0;
    }

    public override void Silence()
    {
      if (!_isInitialized)
        Initialize();
      
      silence();
    }

    public override void SetVolume(int volume)
    {
      if (!_isInitialized)
        Initialize();
      
      setVolume(volume);
    }

    public override int GetVolume()
    {
      if (!_isInitialized)
        Initialize();
      
      return (int)getVolume();
    }

    public override void SetRate(int rate)
    {
      if (!_isInitialized)
        Initialize();
      
      setRate(rate);
    }

    public override int GetRate()
    {
      if (!_isInitialized)
        Initialize();
      
      return (int)getRate();
    }
  }
}