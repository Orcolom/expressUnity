﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExpressVisualizer : MonoBehaviour
{
  RectTransform _targetTransform;
  RectTransform _transform;
  
  bool _isEnabled = false;

  void Start()
  {
    _transform = transform as RectTransform;
  }

  public void SetTarget(RectTransform targetTransform)
  {
    if (_isEnabled)
      gameObject.SetActive(targetTransform != null);

    _targetTransform = targetTransform;
  }

  void Update()
  {
    if (!_isEnabled || _targetTransform == null)
      return;

    // get the actual world size
    Vector3[] points = new Vector3[4];
    _targetTransform.GetWorldCorners(points);
    var size = points[2] - points[0];

    // position and keep up to date
    _transform.position = _targetTransform.position;
    _transform.pivot = _targetTransform.pivot;
    _transform.sizeDelta = size;
  }

  public void SetEnabled(bool state)
  {
    _isEnabled = state;

    if (_isEnabled && _targetTransform != null)
      gameObject.SetActive(true);
  }
}
