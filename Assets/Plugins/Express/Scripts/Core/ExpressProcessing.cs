﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace ExpressPlugin
{
  // object proccessing for express
  public partial class Express
  {
    /// <summary> tries to find the root of an element
    /// <para>It could be that the current gameobject is part of a button or slider. if this is the case we need to work from them and not the current gameobject</para>
    /// </summary>
    /// <param name="go">the gameobject we need to root</param>
    /// <returns>the root gameobject. could be the same as input<returns>
    public static GameObject GetUIElementRoot(GameObject go)
    {
      return GetUIElementRoot(go, true);
    }

    static GameObject GetUIElementRoot(GameObject go, bool isInput)
    {
      if (go == null)
        return go;
      
      var ignore = go.GetComponent<ExpressIgnore>();
      var skipSelf = ignore != null && ignore.IsChildOfElement;

      if (!skipSelf)
      {
        // if the object has an selectable then it is a root
        var selectable = go.GetComponent<Selectable>();
        if (selectable)
          return go;

        var element = go.GetComponent<AExpressElement>();
        if (element)
          return go;
      }

      // if it doesnt have a parent then it returns null
      // this is so that we dont keep looking for parents until we get the canvas
      // happens with image and raw image
      var parent = go.transform.parent;
      if (parent == null)
        return null;
      
      // recursively look upwards for a root
      var newRoot = GetUIElementRoot(parent.gameObject, false); 
      if (newRoot != null)
        return newRoot;
      
      // if newRoot returned null because we found no valid parent then assume this object is the root
      if (isInput)
        return go;
      else
        return null;
    }

    /// <summary> Get the root of the gameobject and speak its metadata </summary>
    /// <param name="go">the gameobject we will speak</param>
    /// <returns>returns if speaking succeeded<returns>
    public static bool SpeakGameObject(GameObject go)
    {
      var data = GetMetaData(go);
      return Express.SpeakUI(data);
    }

    /// <summary>Is this element getting ignored</summary>
    /// <param name="go">the gameobject to check </param>
    /// <returns>returns state<returns>
    public static bool ShouldIgnoreGameObject(GameObject go)
    {
      return go.GetComponent<ExpressIgnore>() != null;
    }

    /// <summary> Get the metadata of an object</summary>
    /// <param name="go">the gameobject to check </param>
    /// <returns>returns the objects metadata<returns>
    public static Metadata GetMetaData(GameObject go)
    {
      Metadata data = new Metadata();
      
      // use the attached metadata before using defaults
      var expressElement = go.GetComponent<AExpressElement>();
      if (expressElement != null)
      {
        data = expressElement.GetMetaData();

        // if filled up return
        if (!string.IsNullOrEmpty(data.Name) &&
            !string.IsNullOrEmpty(data.Info) &&
            !string.IsNullOrEmpty(data.Role) &&
            !string.IsNullOrEmpty(data.State))
          return data;
      }
      

      // check if text 
      var text = go.GetComponent<Text>();
      if (text != null)
      {
        data.SetRoleIfEmpty("label");
        data.SetInfoIfEmpty(text.text);
        return data;
      }

      // check if slider
      var slider = go.GetComponent<Slider>();
      if (slider != null)
      {
        data.SetRoleIfEmpty("slider");
        data.SetStateIfEmpty(slider.value.ToString());
        return data;
      }

      // check if dropdown
      var dropdown = go.GetComponent<Dropdown>();
      if (dropdown != null)
      {
        var val = dropdown.options[dropdown.value];
        
        data.SetRoleIfEmpty("dropdown");
        data.SetStateIfEmpty(string.Format("{0} out of {1}", dropdown.value, Mathf.Max(dropdown.options.Count - 1, 0)));
        data.SetInfoIfEmpty(val.text);
        return data;
      }

      // check if button
      var button = go.GetComponent<Button>();
      if (button != null)
      {
        // presume first found label is its name
        var label = button.GetComponentInChildren<Text>();
        
        data.SetRoleIfEmpty("button");
        if (label != null)
          data.SetNameIfEmpty(label.text);
        return data;
      }

      // check if input
      var input = go.GetComponent<InputField>();
      if (input != null)
      {
        data.SetRoleIfEmpty("input");
        data.SetInfoIfEmpty(input.text);
        return data;
      }

      // check if toggle
      var toggle = go.GetComponent<Toggle>();
      if (toggle != null)
      {
        dropdown = toggle.GetComponentInParent<Dropdown>();
        var label = toggle.GetComponentInChildren<Text>();

        // if it is part of a dropdown act differently
        if (dropdown != null)
        {
          // find the current value and add additional info
          var idx = dropdown.options.FindIndex((Dropdown.OptionData option) => option.text == label.text);
          var val = dropdown.options[idx];

          data.SetNameIfEmpty(val.text);
          data.SetStateIfEmpty(string.Format("{2} - {0} out of {1}", idx + 1, dropdown.options.Count, idx == dropdown.value? "Checked" : "Unchecked"));
        }
        else
        {
          data.SetRoleIfEmpty("toggle");
          data.SetStateIfEmpty(toggle.isOn? "Checked" : "unchecked");

          // presume first found label is info
          if (label != null)
            data.SetInfoIfEmpty(label.text);
        }
        return data;
      }

      // check if image or raw image
      var image = go.GetComponent<Image>();
      var rawImage = go.GetComponent<RawImage>();
      if (image != null || rawImage != null)
      {
        data.SetRoleIfEmpty("image");
        return data;
      }

      // nothing we know, use its gameobject name as name
      data.SetNameIfEmpty(go.name);
      return data;
    }

    public static GameObject FindPrevElement(GameObject go, int offset = 0)
    {
      if (go == null)
        return null;

      var parent = go.transform.parent;
      if (parent == null)
        return null;

      var currentIdx = go.transform.GetSiblingIndex();
      var idx = currentIdx - 1 + offset;
      var childCount = parent.childCount;
      
      // loop around to the first element
      if (idx == -1)
      {
        var result = FindPrevElement(parent.gameObject);
        if (result != null)
          return result;

        idx = childCount - 1;
      }

      for (; idx >= 0; idx--)
      {
        var nextChild = parent.GetChild(idx);
        var nextRoot = Express.GetUIElementRoot(nextChild.gameObject);
        // if next child and root are the same then see if you can use it
        if (nextRoot.transform == nextChild)
        {
          // if ignored continue
          if (nextRoot.GetComponent<ExpressIgnore>() == null)
          {
            // if has children and isnt a selectable look inside
            if (nextChild.transform.childCount > 0)
            {
              // the offset value feels ike a weak system, rethink maybe
              if (nextChild.GetComponent<Selectable>() == null && nextChild.GetComponent<AExpressElement>() == null)
              {
                var val = FindPrevElement(nextRoot.transform.GetChild(nextRoot.transform.childCount - 1).gameObject, 1);
                if (val != null)
                  return val;
              }
            }
            return nextRoot;
          }
        }

        // if same element did loop
        if (idx == currentIdx)
          break;
      }

      return null;
    }
    
    public static GameObject FindFirstElement(GameObject go)
    {
      if (go == null)
        return null;

      if (go.transform.childCount == 0)
        return null;
      
      return FindNextElement(go.transform.GetChild(0).gameObject, - 1);
    }

    public static GameObject FindNextElement(GameObject go, int offset = 0)
    {
      if (go == null)
        return null;

      var parent = go.transform.parent;
      if (parent == null)
        return FindFirstElement(go);
      
      var currentIdx = go.transform.GetSiblingIndex();
      var idx = currentIdx + 1 + offset;
      var childCount = parent.childCount;
      
      for (; idx < childCount; idx++)
      {
        var nextChild = parent.GetChild(idx);
        var nextRoot = Express.GetUIElementRoot(nextChild.gameObject);
        // if next child and root are the same then see if you can use it
        if (nextRoot.transform == nextChild)
        {
          // if ignored continue
          if (nextRoot.GetComponent<ExpressIgnore>() == null)
          {
            // if has children and isnt a selectable look inside
            if (nextChild.transform.childCount > 0)
            {
              // the offset value feels ike a weak system, rethink maybe
              if (nextChild.GetComponent<Selectable>() == null && nextChild.GetComponent<AExpressElement>() == null)
              {
                var val = FindNextElement(nextRoot.transform.GetChild(0).gameObject, -1);
                if (val !=null)
                  return val;
              }
            }
            return nextRoot;
          }
        }

        // if same element did loop
        if (idx == currentIdx)
          break;

        // loop around to the first element
        if (idx == childCount - 1)
          idx = -1;
      }

      return FindNextElement(parent.gameObject);
    }
  }
}