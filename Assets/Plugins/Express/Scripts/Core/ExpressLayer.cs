﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using ExpressPlugin.Internal;

namespace ExpressPlugin
{  
  // express layer manages lifeline and visualizer
  public partial class Express
  {
    static GameObject _layer;
    static ExpressVisualizer _visualizer;

    public static void InitLayer(GameObject prefab)
    {
      _layer = GameObject.Instantiate(prefab);
      _visualizer = _layer.GetComponentInChildren<ExpressVisualizer>();
      _visualizer.gameObject.SetActive(false);
    }
    
    public static void DestroyLayer()
    {
      GameObject.Destroy(_layer);
    }
    
    public static void EnableVisualizer(bool state)
    {
      if (_visualizer != null)
        _visualizer.SetEnabled(state);
    }

    public static void SetVisualizerTarget(RectTransform transform)
    {
      if (_visualizer != null)
        _visualizer.SetTarget(transform);
    }
  }
}