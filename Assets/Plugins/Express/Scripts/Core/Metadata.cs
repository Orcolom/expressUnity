﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ExpressPlugin
{    
  // UI elements typical have 4 important metadata fields 
  [System.Serializable]
  public class Metadata
  {
    public string Role = ""; // the role of the current ui element (button, label, dropdown)
    public string Name = ""; // the name of the element. Short and snappy
    public string Info = ""; // some more in depth info if needed 
    public string State = ""; // its current state, relevant for checkboxes and sliders
  
    /// <summary>Set name if it hasn't been set yet</summary>
    /// <param name="str">string to set</param>  
    public void SetNameIfEmpty(string str)
    {
      if (Name == "")
        Name = str;
    }

    /// <summary>Set role if it hasn't been set yet</summary>
    /// <param name="str">string to set</param>  
    public void SetRoleIfEmpty(string str)
    {
      if (Role == "")
        Role = str;
    }

    /// <summary>Set info if it hasn't been set yet</summary>
    /// <param name="str">string to set</param>  
    public void SetInfoIfEmpty(string str)
    {
      if (Info == "")
        Info = str;
    }

    /// <summary>Set state if it hasn't been set yet</summary>
    /// <param name="str">string to set</param>  
    public void SetStateIfEmpty(string str)
    {
      if (State == "")
        State = str;
    }
  }
}