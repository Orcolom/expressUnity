﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using ExpressPlugin;

namespace ExpressPlugin.Internal
{
  // this exists to make sure that express gets destroyed when application ends.
  // express will auto add this file when initialized
  public class ExpressLifeline : MonoBehaviour
  {
    void Awake()
    {
      DontDestroyOnLoad(gameObject);
      // Express.Initialize(); this gets done by express itself. doing this here will only cause problems 
    }

    private void OnApplicationQuit()
    {
      Express.Destroy();
    }
  }
}
