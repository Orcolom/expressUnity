﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

using ExpressPlugin.Devices;
using ExpressPlugin.Internal;

namespace ExpressPlugin
{

  // this is the life of express
  // it makes sure we use the correct api and that it will destroy the needed elements
  public partial class Express
  {
    public static bool IsInitialized { get { return _isInitialized; }}
    static bool _isInitialized = false;
    static bool _isInitializing = false;

    static ExpressDeviceAPI _deviceApi; // the current api

    /// <summary>Initialize Express
    /// <para>Tries to initialize api</para>
    /// </summary>
    /// <returns>return true if initialization succeeded </returns>
    public static bool Initialize(ExpressSettings settings)
    {
      if (_isInitialized)
        return true;
      
      if (_isInitializing)
        return false;
      _isInitializing = true;

      // set device using defines
      #if UNITY_STANDALONE_WIN
        _deviceApi = new ExpressWindows();
      #endif
      #if UNITY_EDITOR_WIN
      if (_deviceApi == null)
        _deviceApi = new ExpressWindows();          
      #endif
      #if UNITY_ANDROID
      if (_deviceApi == null)
        _deviceApi = new ExpressAndroid();          
      #endif

      // if nothing found then its not a supported platform
      if (_deviceApi == null)
      {
        Debug.LogError("Express: no supported platform selected");
        _isInitializing = false;
        return false; 
      }

      //initialize
      var res = _deviceApi.Initialize();
      _isInitialized = res;

      // only enable lifeline if everything succeeded
      if (res)
      {
        InitLayer(settings.ExpressLayerPrefab);
        Debug.Log("Express: initialization successfull");
      }
      else
        Debug.LogError("Express: initialization failed with a result of " + res);

      _isInitializing = false;

      Express.SetVolume(settings.Volume);
      Express.SetRate(settings.Rate);

      if (settings.ShowVisualizerOnInit)
        Express.EnableVisualizer(true);

      return _isInitialized;
    }

    /// <summary>Destroy Express </summary>
    public static void Destroy()
    {
      if (!_isInitialized)
        return;
      
      // destroy the lifeline and then destroy the api
      DestroyLayer();
      if (_deviceApi != null)
        _deviceApi.Destroy();
      _isInitialized = false;  
    }

    /// <summary>Speak a sentence</summary>
    /// <param name="str">string to speak</param>  
    /// <param name="intercept">interupt current and backlogged strings; will remove those strings</param>  
    /// <returns>return true if succeeded </returns>
    public static bool Speak(string str, bool intercept = false)
    {
      // if not initialized then try to initialize
      if (!_isInitialized)
        return false;
      
      // if we have an api, then push the request
      if (_deviceApi != null)
        return _deviceApi.Speak(str, intercept);

      return false;
    }

    /// <summary>Speak a sentence according to metadata rules. Will always intercept</summary>
    /// <param name="data">ui metadata to follow</param>
    /// <returns>return true if succeeded </returns>
    public static bool SpeakUI(Metadata data)
    {
      if (string.IsNullOrWhiteSpace(data.Name + data.Info + data.Role + data.State))
        return true;

      // format the data in a predictable constant shape
      return Speak(string.Format("{0} - {1} - {2} - {3}", data.Role, data.Name, data.Info, data.State), true);
    }


    public static void Silence()
    {
      if (!_isInitialized)
        return;
      
      if (_deviceApi != null)
        _deviceApi.Silence();
    }

    public static void SetVolume(int volume)
    {
      if (!_isInitialized)
        return;
      
      if (_deviceApi != null)
        _deviceApi.SetVolume(volume);
    }

    public static int GetVolume()
    {
      if (!_isInitialized)
        return 0;
      
      if (_deviceApi != null)
        return _deviceApi.GetVolume();
      return 0;
    }

    public static void SetRate(int rate)
    {
      if (!_isInitialized)
        return;
      
      if (_deviceApi != null)
        _deviceApi.SetRate(rate);
    }

    public static int GetRate()
    {
      if (!_isInitialized)
        return 0;
      
      if (_deviceApi != null)
        return _deviceApi.GetRate();
      return 0;
    }
  }
}