﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ExpressPlugin
{
  [CreateAssetMenu(menuName = "Express Settings")]
  public class ExpressSettings : ScriptableObject
  {
    [Header("Prefabs")]
    public GameObject ExpressLayerPrefab;
    
    [Header("Awake")]
    public bool InitializeExpressOnAwake = true;
    public bool ShowVisualizerOnInit = false;

    [Header("Controls")]
    public bool UseTabbing = false;
    public bool ForceDoubleClick = false;
    public float DoubleClickDelta = 0.3f;

    [Header("Speach")]
    [Range(0, 100)] public int Volume = 50;
    [Range(-10, 10)] public int Rate = 0;
  } 
}
